/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ui;

import beans.Author;
import beans.Reservation;
import beans.User;
import design_patterns.builder.BookProductBuilder;
import design_patterns.builder.ProductCreator;
import design_patterns.builder.VideoProductBuilder;
import design_patterns.facade.RestClient;
import design_patterns.strategy.Product;
import java.awt.Color;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 *
 * @author root
 */
public class List extends javax.swing.JFrame {

    private Login login_window;
    private ArrayList newvideos,newbooks;
    private User user;
    
    /**
     * Creates new form List
     */
    public List() {
        initComponents();
    }

    public List(Login login_window, User user) {
        initComponents();
        this.login_window=login_window;
        this.user = user;
        listProducts();
    }

  private void listProducts(){
        ProductCreator productcreator = new ProductCreator();
        VideoProductBuilder videobuilder = new VideoProductBuilder();
        BookProductBuilder bookbuilder = new BookProductBuilder();
        
        String data = new RestClient().apacheHttpClientGet(Login.url+"api/reservation/video/?format=json", this.user.getUsername(), this.user.getPassword());
        if( data!=null) {
            try {
                this.newvideos = new ArrayList();
                JSONObject jsonObject = (JSONObject) new JSONParser().parse(data);
                for (Object c : (JSONArray) jsonObject.get("objects")){
                    jsonObject = (JSONObject) c;
                    Matcher makeMatch = Pattern.compile("\\d+").matcher((String)jsonObject.get("author"));
                    makeMatch.find();
                    String inputInt = makeMatch.group();
                    Author author = (Author) this.login_window.getAuthors_map().get(Integer.parseInt(inputInt));
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                    Date date=null;
                    try {
                        date = sdf.parse((String) jsonObject.get("pub_date"));
                    } catch (java.text.ParseException ex) {
                        Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
                    }
                 
                    productcreator.setProduct(videobuilder);
                    productcreator.constructProduct(Integer.parseInt((String)jsonObject.get("id")),author,(String) jsonObject.get("name"),(String) jsonObject.get("genre"),date);
                    Product product = productcreator.getProduct();
                    this.newvideos.add(product);
                }
            } catch (ParseException ex) {
                Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        
        data = new RestClient().apacheHttpClientGet(Login.url+"api/reservation/book/?format=json", this.user.getUsername(), this.user.getPassword());
        if( data!=null) {
            try {
                this.newbooks = new ArrayList();
                JSONObject jsonObject = (JSONObject) new JSONParser().parse(data);
                for (Object c : (JSONArray) jsonObject.get("objects")){
                    jsonObject = (JSONObject) c;
                    Matcher makeMatch = Pattern.compile("\\d+").matcher((String)jsonObject.get("author"));
                    makeMatch.find();
                    String inputInt = makeMatch.group();
                    Author author = (Author) this.login_window.getAuthors_map().get(Integer.parseInt(inputInt));
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                    Date date=null;
                    try {
                        date = sdf.parse((String) jsonObject.get("pub_date"));
                    } catch (java.text.ParseException ex) {
                        Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
                    }
                 
                    productcreator.setProduct(bookbuilder);
                    productcreator.constructProduct(Integer.parseInt((String)jsonObject.get("id")),author,(String) jsonObject.get("name"),(String) jsonObject.get("genre"),date);
                    Product product = productcreator.getProduct();
                    this.newbooks.add(product);
                }
            } catch (ParseException ex) {
                Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        Iterator it = this.login_window.getReservations_map().entrySet().iterator();
        Map reservationProducts = new HashMap();
        while (it.hasNext()) {
            Map.Entry pairs = (Map.Entry)it.next();
            Reservation r = (Reservation) pairs.getValue();
            reservationProducts.put(r.getProduct().getId(), r.getProduct());
            //it.remove(); // avoids a ConcurrentModificationException
        }
        
        this.jTextArea1.setForeground(Color.blue);
        
        for (int i=0; i<this.newvideos.size();i++){
            Product video = (Product) this.newvideos.get(i);         
            if(reservationProducts.get(video.getId())!=null){
                this.jTextArea1.setText(this.jTextArea1.getText()+video.getName()+"****** RESERVED"+"\n");
            }
            else{
                this.jTextArea1.setText(this.jTextArea1.getText()+video.getName()+"****** AVAILABLE"+"\n");
            }
        }
        
        for (int i=0; i<newbooks.size();i++){
            Product book = (Product) newbooks.get(i);
            if(reservationProducts.get(book.getId())!=null){
                this.jTextArea1.setText(this.jTextArea1.getText()+book.getName()+"****** RESERVED"+"\n");
            }
            else{
                this.jTextArea1.setText(this.jTextArea1.getText()+book.getName()+"****** AVAILABLE"+"\n");
            }
        }
 
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTextArea1 = new javax.swing.JTextArea();
        jButton1 = new javax.swing.JButton();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenu2 = new javax.swing.JMenu();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(""));

        jTextArea1.setColumns(20);
        jTextArea1.setRows(5);
        jScrollPane1.setViewportView(jTextArea1);

        jButton1.setText("Close");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 350, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(149, 149, 149)
                .addComponent(jButton1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 221, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 11, Short.MAX_VALUE)
                .addComponent(jButton1)
                .addContainerGap())
        );

        jMenu1.setText("File");
        jMenuBar1.add(jMenu1);

        jMenu2.setText("Edit");
        jMenuBar1.add(jMenu2);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        this.dispose();
    }//GEN-LAST:event_jButton1ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(List.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(List.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(List.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(List.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new List().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextArea jTextArea1;
    // End of variables declaration//GEN-END:variables

    /**
     * @return the login_window
     */
    public Login getLogin_window() {
        return login_window;
    }

    /**
     * @param login_window the login_window to set
     */
    public void setLogin_window(Login login_window) {
        this.login_window = login_window;
    }

    /**
     * @return the user
     */
    public User getUser() {
        return user;
    }

    /**
     * @param user the user to set
     */
    public void setUser(User user) {
        this.user = user;
    }
    
    public javax.swing.JTextArea getjTextArea1(){
        return this.jTextArea1;
    }
}
