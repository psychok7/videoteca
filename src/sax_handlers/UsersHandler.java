/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package sax_handlers;

import beans.Author;
import beans.User;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
import ui.Login;

/**
 *
 * @author root
 */
public class UsersHandler extends DefaultHandler {

    private String data;
    private Login login;
    private User u;
    private String temp;
    private ArrayList <User> users;
    
    public UsersHandler(){
        
    }
    
    public UsersHandler(Login login, String data, User u) {
        this.data=data;
        this.login=login;
        this.users=new ArrayList<User>();
        this.login.setUsers_map(new HashMap());
        this.u = u;
        parseDocument();
        //readList();
    }

       /*
        * Every time the parser encounters the beginning of a new element,
        * it calls this method, which resets the string buffer
        */ 
       public void startElement(String uri, String localName,String qName, Attributes attributes) throws SAXException {
              temp = "";
              if (qName.equalsIgnoreCase("Object")) {
                     this.u = new User();
              }
       }
    
       /*
        * When the parser encounters the end of an element, it calls this method
        */
       public void endElement(String uri, String localName, String qName)
                     throws SAXException {

              if (qName.equalsIgnoreCase("Object")) {
                     // add it to the list
                     this.users.add(u);
                     this.login.getUsers_map().put(u.getId(), u);

              } else if (qName.equalsIgnoreCase("Username")) {
                     this.u.setUsername(temp);
              } 
              else if (qName.equalsIgnoreCase("Id")) {
                     this.u.setId(Integer.parseInt(temp));
              }

       }

    public void characters(char[] buffer, int start, int length) {
              temp = new String(buffer, start, length);
       }
 
    private void parseDocument() {

        // parse

        SAXParserFactory factory = SAXParserFactory.newInstance();

        try {
            SAXParser parser = factory.newSAXParser();
            parser.parse(new InputSource(new ByteArrayInputStream(this.data.getBytes("utf-8"))), this);

        } catch (ParserConfigurationException e) {

            System.out.println("ParserConfig error");

        } catch (SAXException e) {

            System.out.println("SAXException : xml not well formed");

        } catch (IOException e) {

            System.out.println("IO error");

        }

    }
       private void readList() {
              System.out.println("Usernames of  the users '" + this.login.getUsers_map().size()  + "'.");
              Iterator<User> it = this.login.getUsers_map().entrySet().iterator();
              while (it.hasNext()) {
                    Map.Entry pairs = (Map.Entry)it.next();
                    System.out.println(pairs.getKey() + " = " + pairs.getValue());
                    //it.remove(); // avoids a ConcurrentModificationException
                }
       }
       
           /**
     * @return the users_map
     */
    public Map getUsers_map() {
        return this.login.getUsers_map();
    }

    /**
     * @return the users
     */
    public ArrayList <User> getUsers() {
        return users;
    }

    /**
     * @param users the users to set
     */
    public void setUsers(ArrayList <User> users) {
        this.users = users;
    }
    
}
