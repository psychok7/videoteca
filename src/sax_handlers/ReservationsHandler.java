/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package sax_handlers;


import beans.Author;
import beans.Reservation;
import beans.User;
import design_patterns.strategy.Product;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
import ui.Login;

/**
 *
 * @author root
 */
public class ReservationsHandler extends DefaultHandler {
    private String data;
    private Login login;
    private User u;
    private Reservation reservation;
    private String temp;
    private String temp_reserv_date_start=null;
    private String temp_reserv_finish = null;

    public ReservationsHandler (Login login, String data, User u) {
        this.data=data;
        this.login=login;
        this.login.setReservations(new ArrayList ());
        this.login.setReservations_map(new HashMap());
        this.u = u;
        parseDocument();
        //readList();
    }

       /*
        * Every time the parser encounters the beginning of a new element,
        * it calls this method, which resets the string buffer
        */ 
       public void startElement(String uri, String localName,String qName, Attributes attributes) throws SAXException {
              temp = "";
              if (qName.equalsIgnoreCase("Object")) {
                     this.reservation = new Reservation();
              }
       }
    
       /*
        * When the parser encounters the end of an element, it calls this method
        */
       public void endElement(String uri, String localName, String qName)
                     throws SAXException {

              if (qName.equalsIgnoreCase("Object")) {
                     // add it to the list
                     if (this.reservation!=null){
                        this.login.getReservations().add(this.reservation);
                        this.login.getReservations_map().put(this.reservation.getId(), this.reservation);
                     }

              }
              else if (qName.equalsIgnoreCase("Reserv_date_start")) {
                     
                     this.temp_reserv_date_start=temp;
              }
              else if (qName.equalsIgnoreCase("Reserv_finish")) {
                     
                     this.temp_reserv_finish=temp;
              }
              else if (qName.equalsIgnoreCase("Product")) {          
                    Matcher makeMatch = Pattern.compile("\\d+").matcher((String)temp);
                    makeMatch.find();
                    String inputInt = makeMatch.group();
                    Product product = (Product) this.login.getProducts_map().get(Integer.parseInt(inputInt));
                    Author author = (Author) login.getAuthors_map().get(product.getAuthor().getId());
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                    Date reserv_date_start=null;
                    Date reserv_finish =null;
                    try {
                        if (reserv_date_start!=null && reserv_finish!=null){
                            reserv_date_start = sdf.parse((String) this.temp_reserv_date_start);
                            reserv_finish = sdf.parse((String) this.temp_reserv_finish);
                        }
                    } catch (java.text.ParseException ex) {
                        Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
                    }

                    this.reservation.setProduct(product);
                    this.reservation.setUser(this.u);
                    this.reservation.setReserv_date_start(reserv_date_start);
                    this.reservation.setReserv_finish(reserv_finish);
                    
              }
              else if (qName.equalsIgnoreCase("Penalty")) {
                     if (temp.equalsIgnoreCase("True")){
                         this.reservation.setPenalty(true);
                     }
                     else{
                         this.reservation.setPenalty(false);
                     }
              } else if (qName.equalsIgnoreCase("Id")) {
                     this.reservation.setId(Integer.parseInt(temp));
              }

       }

    public void characters(char[] buffer, int start, int length) {
              temp = new String(buffer, start, length);
       }
 
    private void parseDocument() {

        // parse

        SAXParserFactory factory = SAXParserFactory.newInstance();

        try {
            SAXParser parser = factory.newSAXParser();
            parser.parse(new InputSource(new ByteArrayInputStream(this.data.getBytes("utf-8"))), this);

        } catch (ParserConfigurationException e) {

            System.out.println("ParserConfig error");

        } catch (SAXException e) {

            System.out.println("SAXException : xml not well formed");

        } catch (IOException e) {

            System.out.println("IO error");

        }

    }    
       private void readList() {
              System.out.println("Names of  the reservations '" + this.login.getReservations().size()  + "'.");
              Iterator<Reservation> it = this.login.getReservations().iterator();
              while (it.hasNext()) {
                  Reservation aux = it.next();
                  System.out.println(aux.getId());
                  System.out.println(aux.isPenalty());
              }      
       }


    
}
