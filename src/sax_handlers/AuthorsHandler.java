/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package sax_handlers;

import beans.Author;
import beans.User;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
import ui.Login;

/**
 *
 * @author root
 */
public class AuthorsHandler extends DefaultHandler {
    
    private String data;
    private Login login;
    private User u;
    private Author author;
    private String temp;

    public AuthorsHandler(Login login, String data, User u) {
        this.data=data;
        this.login=login;
        this.login.setAuthors(new ArrayList ());
        this.login.setAuthors_map(new HashMap());
        this.u = u;
        parseDocument();
        //readList();
    }

       /*
        * Every time the parser encounters the beginning of a new element,
        * it calls this method, which resets the string buffer
        */ 
       public void startElement(String uri, String localName,String qName, Attributes attributes) throws SAXException {
              temp = "";
              if (qName.equalsIgnoreCase("Object")) {
                     this.author = new Author();
              }
       }
    
       /*
        * When the parser encounters the end of an element, it calls this method
        */
       public void endElement(String uri, String localName, String qName)
                     throws SAXException {

              if (qName.equalsIgnoreCase("Object")) {
                     // add it to the list
                     this.login.getAuthors().add(author);
                     this.login.getAuthors_map().put(author.getId(), author);

              } else if (qName.equalsIgnoreCase("Name")) {
                     this.author.setName(temp);
              } else if (qName.equalsIgnoreCase("Email")) {
                     this.author.setEmail(temp);
              } else if (qName.equalsIgnoreCase("Id")) {
                     this.author.setId(Integer.parseInt(temp));
              }

       }

    public void characters(char[] buffer, int start, int length) {
              temp = new String(buffer, start, length);
       }
 
    private void parseDocument() {

        // parse

        SAXParserFactory factory = SAXParserFactory.newInstance();

        try {
            SAXParser parser = factory.newSAXParser();
            parser.parse(new InputSource(new ByteArrayInputStream(this.data.getBytes("utf-8"))), this);

        } catch (ParserConfigurationException e) {

            System.out.println("ParserConfig error");

        } catch (SAXException e) {

            System.out.println("SAXException : xml not well formed");

        } catch (IOException e) {

            System.out.println("IO error");

        }

    }
       private void readList() {
              System.out.println("Emails of  the authors '" + this.login.getAuthors().size()  + "'.");
              Iterator<Author> it = this.login.getAuthors().iterator();
              while (it.hasNext()) {
                  Author aux = it.next();
                  System.out.println(aux.getEmail());
              }
       }


}
