/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package sax_handlers;


import beans.Author;
import beans.User;
import design_patterns.strategy.Product;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
import ui.Login;

/**
 *
 * @author root
 */
public class ProductsHandler extends DefaultHandler {
    private String data;
    private Login login;
    private User u;
    private Product product;
    private String temp;
    private String temp_pub_date=null;

    public ProductsHandler (Login login, String data, User u) {
        this.data=data;
        this.login=login;
        this.login.setProducts(new ArrayList ());
        this.login.setProducts_map(new HashMap());
        this.u = u;
        parseDocument();
        //readList();
    }

       /*
        * Every time the parser encounters the beginning of a new element,
        * it calls this method, which resets the string buffer
        */ 
       public void startElement(String uri, String localName,String qName, Attributes attributes) throws SAXException {
              temp = "";
              if (qName.equalsIgnoreCase("Object")) {
                     this.product = new Product();
              }
       }
    
       /*
        * When the parser encounters the end of an element, it calls this method
        */
       public void endElement(String uri, String localName, String qName)
                     throws SAXException {

              if (qName.equalsIgnoreCase("Object")) {
                     // add it to the list
                     if (this.product!=null){
                        this.login.getProducts().add(product);
                        this.login.getProducts_map().put(product.getId(), product);
                     }

              }
              else if (qName.equalsIgnoreCase("Pub_date")) {
                     
                     this.temp_pub_date=temp;
              }
              else if (qName.equalsIgnoreCase("Author")) {          
                    Matcher makeMatch = Pattern.compile("\\d+").matcher((String)temp);
                    makeMatch.find();
                    String inputInt = makeMatch.group();
                    Author author = (Author) this.login.getAuthors_map().get(Integer.parseInt(inputInt));
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                    Date date=null;
                    try {
                        if (this.temp_pub_date!=null){
                            date = sdf.parse((String) this.temp_pub_date);
                        }
                    } catch (java.text.ParseException ex) {
                        Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    this.product.setAuthor(author);
                    this.product.setPub_date(date);
                    
              }
              else if (qName.equalsIgnoreCase("Name")) {
                     this.product.setName(temp);
              } else if (qName.equalsIgnoreCase("Genre")) {
                     this.product.setGenre(temp);
              } else if (qName.equalsIgnoreCase("Id")) {
                     this.product.setId(Integer.parseInt(temp));
              }
              else if (qName.equalsIgnoreCase("Count")) {
                     this.product.setCount(Long.parseLong(temp));
              }
              

       }

    public void characters(char[] buffer, int start, int length) {
              temp = new String(buffer, start, length);
       }
 
    private void parseDocument() {

        // parse

        SAXParserFactory factory = SAXParserFactory.newInstance();

        try {
            SAXParser parser = factory.newSAXParser();
            parser.parse(new InputSource(new ByteArrayInputStream(this.data.getBytes("utf-8"))), this);

        } catch (ParserConfigurationException e) {

            System.out.println("ParserConfig error");

        } catch (SAXException e) {

            System.out.println("SAXException : xml not well formed");

        } catch (IOException e) {

            System.out.println("IO error");

        }

    }
       private void readList() {
              System.out.println("Names of  the products '" + this.login.getProducts().size()  + "'.");
              Iterator<Product> it = this.login.getProducts().iterator();
              while (it.hasNext()) {
                  Product aux = it.next();
                  System.out.println(aux.getName());
              }
       }


    
}
