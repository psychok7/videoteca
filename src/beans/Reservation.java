/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import design_patterns.strategy.Product;
import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author root
 */
public class Reservation implements Serializable {
    private static final long serialVersionUID = 1L;
    private int id;
    private User user;
    private Product product;
    private Date reserv_date_start;
    private Date reserv_finish;
    private boolean penalty;
    
    public Reservation(){
        
    }
    
    public Reservation(int id, User user, Product product, Date reserv_date_start,Date reserv_finish,boolean penalty){
        this.id = id;
        this.user = user;
        this.product = product;
        this.reserv_date_start = reserv_date_start;
        this.reserv_finish = reserv_finish;
        this.penalty = penalty;
    }

    /**
     * @return the user
     */
    public User getUser() {
        return user;
    }

    /**
     * @param user the user to set
     */
    public void setUser(User user) {
        this.user = user;
    }

    /**
     * @return the product
     */
    public Product getProduct() {
        return product;
    }

    /**
     * @param product the product to set
     */
    public void setProduct(Product product) {
        this.product = product;
    }

    /**
     * @return the reserv_date_start
     */
    public Date getReserv_date_start() {
        return reserv_date_start;
    }

    /**
     * @param reserv_date_start the reserv_date_start to set
     */
    public void setReserv_date_start(Date reserv_date_start) {
        this.reserv_date_start = reserv_date_start;
    }

    /**
     * @return the reserv_finish
     */
    public Date getReserv_finish() {
        return reserv_finish;
    }

    /**
     * @param reserv_finish the reserv_finish to set
     */
    public void setReserv_finish(Date reserv_finish) {
        this.reserv_finish = reserv_finish;
    }

    /**
     * @return the penalty
     */
    public boolean isPenalty() {
        return penalty;
    }

    /**
     * @param penalty the penalty to set
     */
    public void setPenalty(boolean penalty) {
        this.penalty = penalty;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }
    
    
}
