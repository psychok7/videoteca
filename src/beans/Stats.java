/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

/**
 *
 * @author root
 */
public class Stats {
    
    private String product_name;
    private int id;
    private long count;
    private float percen;
    
    public Stats(int id, String product_url,long count){
        this.id = id;
        this.product_name=product_url;
        this.count = count;
    }
    
    public float getPercentage(float percentage, float x, float y){
        return (float) (x * percentage) / y;      
    }

    /**
     * @return the product_url
     */
    public String getProduct_url() {
        return product_name;
    }

    /**
     * @param product_url the product_url to set
     */
    public void setProduct_url(String product_url) {
        this.product_name = product_url;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the count
     */
    public long getCount() {
        return count;
    }

    /**
     * @param count the count to set
     */
    public void setCount(int count) {
        this.count = count;
    }

    /**
     * @return the percentage
     */
    public float getPercen() {
        return percen;
    }

    /**
     * @param percentage the percentage to set
     */
    public void setPercen(float percentage) {
        this.percen = percentage;
    }
    
    
}
