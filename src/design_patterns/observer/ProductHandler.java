/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package design_patterns.observer;

import java.util.Observable;
import java.util.Observer;  /* this is Event Handler */
import javax.swing.JOptionPane;
 
public class ProductHandler implements Observer {
    private String resp;
    @Override
    public void update(Observable obj, Object arg) {
        if (arg instanceof String) {
            resp = (String) arg;
            if (resp.equals("There is a product update")){
                JOptionPane.showMessageDialog(null, "There is a product update, plese check the products tab");
            }
        }
    }
}
