/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package design_patterns.command;

import beans.User;

/**
 *
 * @author root
 */
public class VideoStats_Command implements Stats_Command{
    private Stats_Receiver theStats_Receiver;
    
    public VideoStats_Command(Stats_Receiver theStats_Receiver){
        this.theStats_Receiver=theStats_Receiver;
    }
    
    @Override
    public void productStats(User u) {
        this.theStats_Receiver.getVideoStats(u);
    }
    
}
