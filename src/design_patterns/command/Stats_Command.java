/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package design_patterns.command;

import beans.User;

/**
 *
 * @author root
 */
public interface Stats_Command {
    void productStats(User u);
}
