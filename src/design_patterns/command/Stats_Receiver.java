/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package design_patterns.command;

import beans.Stats;
import beans.User;
import design_patterns.facade.RestClient;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import ui.List;
import ui.Login;

/**
 *
 * @author root
 */
public class Stats_Receiver {
    private List print;
    
    public Stats_Receiver(){
        this.print = new List();
        this.print.setVisible(true);
    }
    
    public void getVideoStats(User u){
        System.out.println("videostats");
        long total_count = 0;
        ArrayList <Stats> stats_array = new ArrayList <Stats> ();
        String data = new RestClient().apacheHttpClientGet(Login.url+"api/reservation/video/?format=json", u.getUsername(), u.getPassword());
            if( data!=null) {
                try {
                    JSONObject jsonObject = (JSONObject) new JSONParser().parse(data);
                    for (Object c : (JSONArray) jsonObject.get("objects")){
                        jsonObject = (JSONObject) c;
                        total_count = total_count + (long) jsonObject.get("count");
                        Stats stats = new Stats(Integer.parseInt((String)jsonObject.get("id")),(String) jsonObject.get("name"),(long) jsonObject.get("count"));
                        stats_array.add(stats);
                    }
                } catch (ParseException ex) {
                    Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        
        this.print.getjTextArea1().setText("************STATISTICS FOR MOVIES *******************" + "\n\n");
        //get percentage
        for (int i=0; i<stats_array.size();i++){
            Stats aux= (Stats)  stats_array.get(i);
            aux.setPercen(aux.getPercentage((float)100, aux.getCount(), total_count));
            this.print.getjTextArea1().setText(this.print.getjTextArea1().getText() + aux.getProduct_url() +" : " + aux.getPercen() + " % " +"\n");
        }
    }
    
    public void getBookStats(User u){
        System.out.println("bookstats");
        
        long total_count = 0;
        ArrayList <Stats> stats_array = new ArrayList <Stats> ();
        String data = new RestClient().apacheHttpClientGet(Login.url+"api/reservation/book/?format=json", u.getUsername(), u.getPassword());
            if( data!=null) {
                try {
                    JSONObject jsonObject = (JSONObject) new JSONParser().parse(data);
                    for (Object c : (JSONArray) jsonObject.get("objects")){
                        jsonObject = (JSONObject) c;
                        total_count = total_count + (long) jsonObject.get("count");
                        Stats stats = new Stats(Integer.parseInt((String)jsonObject.get("id")),(String) jsonObject.get("name"),(long) jsonObject.get("count"));
                        stats_array.add(stats);
                    }
                } catch (ParseException ex) {
                    Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        this.print.getjTextArea1().setText(this.print.getjTextArea1().getText() + "\n\n" +"************STATISTICS FOR BOOKS *******************" + "\n\n");
            
        //get percentage
        for (int i=0; i<stats_array.size();i++){
            Stats aux= (Stats)  stats_array.get(i);
            aux.setPercen(aux.getPercentage((float)100, aux.getCount(), total_count));
            this.print.getjTextArea1().setText(this.print.getjTextArea1().getText() + aux.getProduct_url() +" : " + aux.getPercen() + " % " +"\n");
        }      
    }
}
