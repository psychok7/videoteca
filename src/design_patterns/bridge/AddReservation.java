/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package design_patterns.bridge;

import beans.User;
import design_patterns.strategy.Product;
import beans.Reservation;

/**
 *
 * @author root
 */

//Refined Abstraction
public class AddReservation extends ManageReservation{
    
    public AddReservation(Product product,User u){
        super(new AddReservationAPI());
        this.product=product;
        this.u = u;
    }
    
    @Override
    public Reservation startReservationManagement(int nextID){
        return this.reservationAPI.startReservationManagement(nextID,this.product,this.u);

    }
}
