/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package design_patterns.bridge;

import beans.Reservation;
import beans.User;
import design_patterns.facade.RestClient;
import design_patterns.strategy.Book;
import design_patterns.strategy.Product;
import design_patterns.strategy.Video;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.json.simple.JSONObject;
import ui.Login;

/**
 *
 * @author root
 */

//ConcreteImplementor
public class AddReservationAPI implements ReservationAPI{

    public JSONObject encodeJsonObject(Reservation reservation){
        JSONObject obj=new JSONObject();
        obj.put("id",String.valueOf(reservation.getId()));  
        obj.put("product","/api/reservation/product/"+reservation.getProduct().getId()+"/");      
        obj.put("reserv_date_start",new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'+00:00'").format(reservation.getReserv_date_start()));
        obj.put("reserv_finish",new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'+00:00'").format(reservation.getReserv_finish()));        
        obj.put("resource_uri", "/api/reservation/reservation/"+reservation.getId()+"/");
        obj.put("user", "/api/reservation/auth/user/"+reservation.getUser().getId()+"/");
        obj.put("penalty",reservation.isPenalty());
        return obj;
    }
    
    public JSONObject encodeJsonObjectProductstats(Reservation reservation){
        JSONObject obj=new JSONObject();
        obj.put("count",(int)(reservation.getProduct().getCount() + 1));
        return obj;
    }
    
    @Override
    public Reservation startReservationManagement(int nextID,Product product, User u) {
        System.out.println("next id: " + nextID);
        RestClient add_reservationto_server = new RestClient();
        
        if (product instanceof Video){
            Reservation reservation = new Reservation(nextID,u,product,new Date(),new Date(),false);
            //System.out.println("json: " + encodeJsonObject(reservation).toJSONString());
            //System.out.println("json: " + encodeJsonObjectProductstats(reservation).toJSONString());            
            add_reservationto_server.apacheHttpClientPost(Login.url+"api/reservation/reservation/", reservation.getUser().getUsername(), reservation.getUser().getPassword(), encodeJsonObject(reservation));
            add_reservationto_server.apacheHttpClientPatch(Login.url+"api/reservation/product/"+reservation.getProduct().getId()+"/", reservation.getUser().getUsername(), reservation.getUser().getPassword(), encodeJsonObjectProductstats(reservation));
            return reservation;
        }
        else if (product instanceof Book ){
            Reservation reservation = new Reservation(nextID,u,product,new Date(),new Date(),false);
            //System.out.println(encodeJsonObject(reservation).toJSONString());
            add_reservationto_server.apacheHttpClientPost(Login.url+"api/reservation/reservation/", reservation.getUser().getUsername(), reservation.getUser().getPassword(), encodeJsonObject(reservation));
            add_reservationto_server.apacheHttpClientPatch(Login.url+"api/reservation/product/"+reservation.getProduct().getId()+"/", reservation.getUser().getUsername(), reservation.getUser().getPassword(), encodeJsonObjectProductstats(reservation));
            return reservation;
        }
        return null;
    }
    
}
