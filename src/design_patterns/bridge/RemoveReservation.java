/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package design_patterns.bridge;

import beans.User;
import design_patterns.strategy.Product;

/**
 *
 * @author root
 */
public class RemoveReservation extends ManageReservation{
    
        public RemoveReservation(Product product,User u){
        super(new RemoveReservationAPI());
        this.product=product;
        this.u= u;
    }
}
