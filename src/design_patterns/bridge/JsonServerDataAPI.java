/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package design_patterns.bridge;

import beans.Author;
import beans.Reservation;
import beans.User;
import design_patterns.facade.RestClient;
import design_patterns.strategy.Book;
import design_patterns.strategy.Product;
import design_patterns.strategy.Video;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import ui.Login;
import ui.Menu;

/**
 *
 * @author root
 */

//ConcreteImplementor
public class JsonServerDataAPI implements ServerDataAPI{
 
    @Override
    public boolean fetchdata(Login login, javax.swing.JTextField username,javax.swing.JPasswordField password) {    

            boolean flag=false;
            User u = new User();

            String data = new RestClient().apacheHttpClientGet(Login.url+"api/reservation/auth/user/?format=json", username.getText(), password.getPassword());
            if( data!=null) {
                try {
                    u.setUsername(username.getText());
                    u.setPassword(password.getPassword());
                    login.setUsers_map(new HashMap());
                    JSONObject jsonObject = (JSONObject) new JSONParser().parse(data);
                    for (Object c : (JSONArray) jsonObject.get("objects")){
                        jsonObject = (JSONObject) c;
                        User remoteuser = new User(Integer.parseInt((String)jsonObject.get("id")),(String) jsonObject.get("username"));                
                        login.getUsers_map().put(remoteuser.getUsername(), remoteuser);
                    }
                } catch (ParseException ex) {
                    Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
                }
                flag = true;
            }
            else {         
                flag=false;
            }

            User temp = (User) login.getUsers_map().get(u.getUsername());
            if (temp!=null){
                u=temp;
                u.setPassword(password.getPassword());
            }
            
            data = new RestClient().apacheHttpClientGet(Login.url+"api/reservation/author/?format=json", username.getText(), password.getPassword());
            if( data!=null) {
                try {
                    login.setAuthors(new ArrayList ());
                    login.setAuthors_map(new HashMap());
                    JSONObject jsonObject = (JSONObject) new JSONParser().parse(data);
                    for (Object c : (JSONArray) jsonObject.get("objects")){
                        jsonObject = (JSONObject) c;
                        Author author = new Author(Integer.parseInt((String)jsonObject.get("id")),(String) jsonObject.get("name"),(String) jsonObject.get("email"));
                        login.getAuthors().add(author);                   
                        login.getAuthors_map().put(author.getId(), author);
                    }
                } catch (ParseException ex) {
                    Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
                }
                flag = true;
            }
            else {         
                flag=false;
            }

            data = new RestClient().apacheHttpClientGet(Login.url+"api/reservation/product/?format=json", username.getText(), password.getPassword());
            if( data!=null) {
                try {
                    login.setProducts(new ArrayList ());
                    login.setProducts_map(new HashMap());
                    JSONObject jsonObject = (JSONObject) new JSONParser().parse(data);
                    for (Object c : (JSONArray) jsonObject.get("objects")){
                        jsonObject = (JSONObject) c;
                        Matcher makeMatch = Pattern.compile("\\d+").matcher((String)jsonObject.get("author"));
                        makeMatch.find();
                        String inputInt = makeMatch.group();
                        Author author = (Author) login.getAuthors_map().get(Integer.parseInt(inputInt));
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                        Date date=null;
                        try {
                            date = sdf.parse((String) jsonObject.get("pub_date"));
                        } catch (java.text.ParseException ex) {
                            Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        Product product = new Product(Integer.parseInt((String)jsonObject.get("id")),author,(String) jsonObject.get("name"),(String) jsonObject.get("genre"),date);
                        product.setCount((long) jsonObject.get("count"));
                        if (product!=null){
                            login.getProducts().add(product);                    
                            login.getProducts_map().put(product.getId(), product);
                        } 
                    }
                } catch (ParseException ex) {
                    Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
                }
                flag = true;
            }
            else {
                flag = false;
            }

            data = new RestClient().apacheHttpClientGet(Login.url+"api/reservation/video/?format=json", username.getText(), password.getPassword());
            if( data!=null) {
                try {
                    login.setVideos(new ArrayList ());
                    login.setVideos_map(new HashMap());
                    JSONObject jsonObject = (JSONObject) new JSONParser().parse(data);
                    for (Object c : (JSONArray) jsonObject.get("objects")){
                        jsonObject = (JSONObject) c;
                        Author author = (Author) login.getAuthors_map().get(Integer.parseInt((String)jsonObject.get("id")));
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                        Date date=null;
                        try {
                            date = sdf.parse((String) jsonObject.get("pub_date"));
                        } catch (java.text.ParseException ex) {
                            Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        Video video = new Video (Integer.parseInt((String)jsonObject.get("id")),author,(String) jsonObject.get("name"),(String) jsonObject.get("genre"),date);
                        video.setCount((long) jsonObject.get("count"));
                        login.getVideos().add(video);               
                        login.getVideos_map().put(video.getId(), video);
                    }
                } catch (ParseException ex) {
                    Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
                }
                flag = true;
            }
            else {
                flag = false;
            }

            data = new RestClient().apacheHttpClientGet(Login.url+"api/reservation/book/?format=json", username.getText(), password.getPassword());
            if( data!=null) {
                try {
                    login.setBooks(new ArrayList ());
                    login.setBooks_map(new HashMap());
                    JSONObject jsonObject = (JSONObject) new JSONParser().parse(data);
                    for (Object c : (JSONArray) jsonObject.get("objects")){
                        jsonObject = (JSONObject) c;
                        Author author = (Author) login.getAuthors_map().get(Integer.parseInt((String)jsonObject.get("id")));
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                        Date date=null;
                        try {
                            date = sdf.parse((String) jsonObject.get("pub_date"));
                        } catch (java.text.ParseException ex) {
                            Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        Book book = new Book (Integer.parseInt((String)jsonObject.get("id")),author,(String) jsonObject.get("name"),(String) jsonObject.get("genre"),date);
                        book.setCount((long) jsonObject.get("count"));
                        login.getBooks().add(book);                  
                        login.getBooks_map().put(book.getId(), book);
                    }
                } catch (ParseException ex) {
                    Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
                }
                flag = true;
            }
            else {
                flag = false;
            }

            data = new RestClient().apacheHttpClientGet(Login.url+"api/reservation/reservation/?format=json", username.getText(), password.getPassword());
            if( data!=null) {
                try {
                    login.setReservations(new ArrayList ());
                    login.setReservations_map(new HashMap());
                    JSONObject jsonObject = (JSONObject) new JSONParser().parse(data);
                    for (Object c : (JSONArray) jsonObject.get("objects")){
                        jsonObject = (JSONObject) c;
                        System.out.println((String)jsonObject.get("user"));
                        Matcher makeMatch = Pattern.compile("\\d+").matcher((String)jsonObject.get("product"));
                        makeMatch.find();
                        String inputInt = makeMatch.group();
                        Product product = (Product) login.getProducts_map().get(Integer.parseInt(inputInt));
                        Author author = (Author) login.getAuthors_map().get(product.getAuthor().getId());
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                        Date reserv_date_start=null;
                        Date reserv_finish =null;
                        try {
                            reserv_date_start = sdf.parse((String) jsonObject.get("reserv_date_start"));
                            reserv_finish = sdf.parse((String) jsonObject.get("reserv_finish"));
                        } catch (java.text.ParseException ex) {
                            Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        Reservation reservation = new Reservation(Integer.parseInt((String)jsonObject.get("id")), u, product, reserv_date_start,reserv_finish, (boolean) jsonObject.get("penalty"));
                        login.getReservations().add(reservation);
                        login.getReservations_map().put(reservation.getId(), reservation);
                    }
                } catch (ParseException ex) {
                    Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
                }
                flag = true;
            }
            else {
                flag = false;
            }

            if (flag) {
                new Menu(login,u).setVisible(true);
                return true;
            }
            else{
                return false;
            }
    }
    
}
