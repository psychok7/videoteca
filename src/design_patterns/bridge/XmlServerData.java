/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package design_patterns.bridge;

import ui.Login;

/**
 *
 * @author root
 */
public class XmlServerData extends ManageServerData{
    
    public XmlServerData (Login login, javax.swing.JTextField username,javax.swing.JPasswordField password){
        super(new XmlServerDataAPI());
        this.login=login;
        this.username=username;
        this.password=password;
    }
    
    @Override
    public boolean fetchdata(){
        return this.serverdataAPI.fetchdata(this.login, this.username, this.password);
    }
    
    
}
