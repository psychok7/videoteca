/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package design_patterns.bridge;

import beans.Reservation;
import beans.User;
import design_patterns.strategy.Book;
import design_patterns.strategy.Product;
import design_patterns.strategy.Video;
import java.util.Date;

/**
 *
 * @author root
 */

//ConcreteImplementor
public class RemoveReservationAPI implements ReservationAPI {

     @Override
    public Reservation startReservationManagement(int nextID,Product product, User u) {
        if (product instanceof Video){
            return new Reservation(nextID,u,product,new Date(),new Date(),false);
        }
        else if (product instanceof Book ){
            return new Reservation(nextID,u,product,new Date(),new Date(),false);
        }
        return null;
    }
    
}
