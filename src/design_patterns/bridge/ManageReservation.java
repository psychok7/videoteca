/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package design_patterns.bridge;

import beans.User;
import design_patterns.strategy.Product;
import beans.Reservation;

/**
 *
 * @author root
 */

//Abstraction
abstract class ManageReservation {
    
    protected ReservationAPI reservationAPI;
    
    protected Product product;
    protected Reservation reservation;
    protected User u;
    
    public ManageReservation(ReservationAPI reservationAPI){
        this.reservationAPI=reservationAPI;
    }
    public ManageReservation(Product product,User u,ReservationAPI reservationAPI){
        this.product=product;
        this.u = u;
        this.reservationAPI = reservationAPI;
    }
    
    public Reservation startReservationManagement(int nextID){
        return null;
    }
    
        
}
