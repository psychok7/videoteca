/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package design_patterns.bridge;

import ui.Login;

/**
 *
 * @author root
 */

//Abstraction
abstract class ManageServerData {
    
    protected ServerDataAPI serverdataAPI;
    
    protected Login login;
    protected javax.swing.JTextField username;
    protected javax.swing.JPasswordField password;
    
    public ManageServerData(ServerDataAPI serverdataAPI){
        this.serverdataAPI=serverdataAPI;       
    }
    
    public boolean fetchdata(){
        return false;
    }

}
