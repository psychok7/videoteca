/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package design_patterns.bridge;

import beans.Reservation;
import beans.User;
import design_patterns.strategy.Product;

/**
 *
 * @author root
 */
//implementor
interface ReservationAPI {
    Reservation startReservationManagement(int nextID,Product product,User user);
}
