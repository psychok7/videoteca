/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package design_patterns.bridge;

import beans.User;
import design_patterns.facade.RestClient;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import sax_handlers.AuthorsHandler;
import sax_handlers.BooksHandler;
import sax_handlers.ProductsHandler;
import sax_handlers.ReservationsHandler;
import sax_handlers.UsersHandler;
import sax_handlers.VideosHandler;
import ui.Login;
import ui.Menu;


/**
 *
 * @author root
 */
public class XmlServerDataAPI implements ServerDataAPI {

    @Override
    public boolean fetchdata(Login login, JTextField username, JPasswordField password) {

        boolean flag=false;
        User u = new User();
        UsersHandler usersHandler=null;
        String data = new RestClient().apacheHttpClientGet(Login.url+"api/reservation/auth/user/?format=xml", username.getText(), password.getPassword());
        if( data!=null) {
            u.setUsername(username.getText());
            u.setPassword(password.getPassword());
            usersHandler = new UsersHandler(login, data, u);        
            flag = true;
        }
        else {         
            flag=false;
        }

        for (int i=0 ; i<usersHandler.getUsers().size();i++){
            User utemp = (User) usersHandler.getUsers().get(i);
            
            if (utemp.getUsername().equalsIgnoreCase(username.getText())){
                u=utemp;
                u.setPassword(password.getPassword());
                break;
            }
        }

        data = new RestClient().apacheHttpClientGet(Login.url+"api/reservation/author/?format=xml", username.getText(), password.getPassword());
        if( data!=null) {
            u.setUsername(username.getText());
            u.setPassword(password.getPassword());
            AuthorsHandler authorsHandler = new AuthorsHandler(login, data, u);
            flag = true;
        }
        else {         
                flag=false;
        }
        
        data = new RestClient().apacheHttpClientGet(Login.url+"api/reservation/product/?format=xml", username.getText(), password.getPassword());
        if( data!=null) {
            ProductsHandler productsHandler = new ProductsHandler(login, data, u);
            flag = true;
        }
        else {         
                flag=false;
        }
        
        data = new RestClient().apacheHttpClientGet(Login.url+"api/reservation/video/?format=xml", username.getText(), password.getPassword());
        if( data!=null) {
            VideosHandler videosHandler = new VideosHandler(login, data, u);
            flag = true;
        }
        else {         
                flag=false;
        }
        
        data = new RestClient().apacheHttpClientGet(Login.url+"api/reservation/book/?format=xml", username.getText(), password.getPassword());
        if( data!=null) {
            BooksHandler booksHandler = new BooksHandler(login, data, u);
            flag = true;
        }
        else {         
                flag=false;
        }
        
        data = new RestClient().apacheHttpClientGet(Login.url+"api/reservation/reservation/?format=xml", username.getText(), password.getPassword());
        if( data!=null) {
            ReservationsHandler reservationsHandler = new ReservationsHandler(login, data, u);
            flag = true;
        }
        else {         
            flag=false;
        }
        
        
        if (flag) {
                new Menu(login,u).setVisible(true);
                return true;
            }
        else{
            return false;
        }
    }
    
}
