/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package design_patterns.facade;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import org.apache.http.HttpException;
import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPatch;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.json.simple.JSONObject;

/**
 *
 * @author root
 */
public class RestClient {

    	public String apacheHttpClientGet(String url, String user, char[] pass) {
	  try {
                      
		DefaultHttpClient httpClient = new DefaultHttpClient();
                httpClient.getCredentialsProvider().setCredentials(new AuthScope(AuthScope.ANY_HOST, AuthScope.ANY_PORT, AuthScope.ANY_REALM),
                new UsernamePasswordCredentials(user, new String(pass)));
		HttpGet getRequest = new HttpGet(url);
		getRequest.addHeader("accept", "application/json");
 
		HttpResponse response = httpClient.execute(getRequest);
 
                
                if (response.getStatusLine().getStatusCode() != 200 && response.getStatusLine().getStatusCode()!=401) {
			throw new RuntimeException("Failed : HTTP error code : "
			   + response.getStatusLine().getStatusCode());
		}
 
		BufferedReader br = new BufferedReader(
                         new InputStreamReader((response.getEntity().getContent())));
 
		String output;
                StringBuilder sb = new StringBuilder();
		//System.out.println("Output from Server .... \n");
		while ((output = br.readLine()) != null) {
                        sb.append(output);
		}
                //System.out.println(sb.toString());
		httpClient.getConnectionManager().shutdown();
                
                return sb.toString();
 
	  } catch (ClientProtocolException e) {
 
		e.printStackTrace();
 
	  } catch (IOException e) {
 
		e.printStackTrace();
	  }
        
          return null;
 
             
	}
    
    public static boolean doDelete(String url, String user, char[] pass) throws HttpException,IOException, URISyntaxException {
        DefaultHttpClient httpClient = new DefaultHttpClient();
        httpClient.getCredentialsProvider().setCredentials(new AuthScope(AuthScope.ANY_HOST, AuthScope.ANY_PORT, AuthScope.ANY_REALM),
        new UsernamePasswordCredentials(user, new String(pass)));
        HttpConnectionParams.setConnectionTimeout(httpClient.getParams(), 10000);
        HttpDelete httpDelete = new HttpDelete(url);
        httpDelete.addHeader("accept", "application/json");

        HttpResponse response = httpClient.execute(httpDelete);

        int statusCode = response.getStatusLine().getStatusCode();

        return statusCode == 200 ? true : false;

    }
 
    
    public void apacheHttpClientPost(String url, String user, char[] pass, JSONObject data) {
         try {
 
		DefaultHttpClient httpClient = new DefaultHttpClient();
                httpClient.getCredentialsProvider().setCredentials(new AuthScope(AuthScope.ANY_HOST, AuthScope.ANY_PORT, AuthScope.ANY_REALM),
                new UsernamePasswordCredentials(user, new String(pass)));
		HttpPost postRequest = new HttpPost(url);
		StringEntity input = new StringEntity(data.toString());
		input.setContentType("application/json");
		postRequest.setEntity(input);
 
		HttpResponse response = httpClient.execute(postRequest);
 
		if (response.getStatusLine().getStatusCode() != 201) {
			throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatusLine().getStatusCode());
		}
 
		BufferedReader br = new BufferedReader(
                        new InputStreamReader((response.getEntity().getContent())));
 
		String output;
		System.out.println("Output from Server .... \n");
		while ((output = br.readLine()) != null) {
			System.out.println(output);
		}
 
		httpClient.getConnectionManager().shutdown();
 
	  } catch (MalformedURLException e) {
 
		e.printStackTrace();
 
	  } catch (IOException e) {
 
		e.printStackTrace();
 
	  }

    }
    
        public void apacheHttpClientPatch(String url, String user, char[] pass, JSONObject data) {
         try {
 
		DefaultHttpClient httpClient = new DefaultHttpClient();
                httpClient.getCredentialsProvider().setCredentials(new AuthScope(AuthScope.ANY_HOST, AuthScope.ANY_PORT, AuthScope.ANY_REALM),
                new UsernamePasswordCredentials(user, new String(pass)));
		HttpPatch patchRequest = new HttpPatch(url);

		StringEntity input = new StringEntity(data.toString());
		input.setContentType("application/json");
                System.out.println("data: "+data.toString());
		patchRequest.setEntity(input);
 
		HttpResponse response = httpClient.execute(patchRequest);

		BufferedReader br = new BufferedReader(
                        new InputStreamReader((response.getEntity().getContent())));
 
		String output;
		System.out.println("Output from Server .... \n");
		while ((output = br.readLine()) != null) {
			System.out.println(output);
		}
 
		httpClient.getConnectionManager().shutdown();
 
	  } catch (MalformedURLException e) {
 
		e.printStackTrace();
 
	  } catch (IOException e) {
 
		e.printStackTrace();
 
	  }

    }
}
