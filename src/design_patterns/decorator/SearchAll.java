/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package design_patterns.decorator;

import design_patterns.strategy.Product;
import java.util.ArrayList;
import javax.swing.tree.DefaultMutableTreeNode;

/**
 *
 * @author root
 */
public class SearchAll implements SearchAPI    {

    @Override
    public DefaultMutableTreeNode search(ArrayList products) {
        DefaultMutableTreeNode products_node = new DefaultMutableTreeNode("Products");
        for (int i = 0; i < products.size();i++){
            Product temp = (Product)products.get(i);
            if (temp!=null){
                products_node.add(new javax.swing.tree.DefaultMutableTreeNode(temp.getName()));
            }
        }
        return products_node;
    }
    
}
