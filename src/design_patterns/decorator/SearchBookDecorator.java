/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package design_patterns.decorator;

import design_patterns.strategy.Book;
import java.util.ArrayList;
import javax.swing.tree.DefaultMutableTreeNode;

/**
 *
 * @author root
 */
public class SearchBookDecorator extends SearchDecorator{

    public SearchBookDecorator(SearchAPI decoratedSearch) {
        super(decoratedSearch);
    }

    @Override
    public DefaultMutableTreeNode search(ArrayList products) {
        this.decoratedSearch.search(products);  //not doing anything
        
        DefaultMutableTreeNode books_node = new DefaultMutableTreeNode("Books");
        for (int i = 0; i < products.size();i++){
            Book temp = (Book)products.get(i);
            if (temp!=null){
                books_node.add(new javax.swing.tree.DefaultMutableTreeNode(temp.getName()));
            }
        }
        
        return books_node;
        
        
    }

    
}
