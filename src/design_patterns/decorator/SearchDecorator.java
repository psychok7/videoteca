/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package design_patterns.decorator;

import java.util.ArrayList;
import javax.swing.tree.DefaultMutableTreeNode;

/**
 *
 * @author root
 */
abstract class SearchDecorator implements SearchAPI {
    protected SearchAPI decoratedSearch;

    public SearchDecorator(SearchAPI decoratedSearch) {
        this.decoratedSearch = decoratedSearch;
    }
    
    @Override
    public DefaultMutableTreeNode search(ArrayList products) {
        return this.decoratedSearch.search(products);
    }
    
}
