/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package design_patterns.decorator;

import java.util.ArrayList;
import javax.swing.tree.DefaultMutableTreeNode;

/**
 *
 * @author root
 */
interface SearchAPI {
    DefaultMutableTreeNode search(ArrayList products);
}
