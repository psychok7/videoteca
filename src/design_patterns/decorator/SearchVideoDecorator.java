/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package design_patterns.decorator;

import design_patterns.strategy.Video;
import java.util.ArrayList;
import javax.swing.tree.DefaultMutableTreeNode;

/**
 *
 * @author root
 */
public class SearchVideoDecorator extends SearchDecorator{
    
    public SearchVideoDecorator(SearchAPI decoratedSearch) {
        super(decoratedSearch);
    }

    @Override
    public DefaultMutableTreeNode search(ArrayList products) {
        this.decoratedSearch.search(products);  //not doing anything
        
        DefaultMutableTreeNode videos_node = new DefaultMutableTreeNode("Videos");
        for (int i = 0; i < products.size();i++){
            Video temp = (Video)products.get(i);
            if (temp!=null){
                videos_node.add(new javax.swing.tree.DefaultMutableTreeNode(temp.getName()));
            }
        }
        
        return videos_node;
    }


    
}
