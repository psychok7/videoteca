/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package design_patterns.strategy;

import beans.Reservation;
import beans.User;
import design_patterns.bridge.AddReservation;

/**
 *
 * @author root
 */


    //Concrete Strategy 1

    class VideoReservation implements IReservation{

        @Override
        public Reservation startReservationManagement(int nextID,Product product,User u){
            AddReservation reservation = new AddReservation(product,u);
            return reservation.startReservationManagement(nextID);
        }
    }
