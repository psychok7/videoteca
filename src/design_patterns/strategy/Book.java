/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package design_patterns.strategy;

import beans.Author;
import java.util.Date;

/**
 *
 * @author root
 */
public class Book extends Product {
    
    public Book() {
        strategy_pattern = new BookReservation();
    } 
    public Book (int id, Author author, String name, String genre, Date pub_date){
        super(id, author, name, genre, pub_date);
        strategy_pattern = new BookReservation();
    }

   
}
