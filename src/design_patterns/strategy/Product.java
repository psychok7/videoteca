/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package design_patterns.strategy;

import beans.Author;
import beans.Reservation;
import beans.User;
import java.io.Serializable;
import java.util.Date;


/**
 *
 * @author root
 */


public class Product implements Serializable {
    protected static long serialVersionUID = 1L;
    protected int id;
    protected long count;
    protected Author author;
    protected String name;
    protected String genre;
    protected Date pub_date;
 
    public IReservation strategy_pattern;

    public Product(){
        
    }
    public Product(int id, Author author, String name, String genre, Date pub_date){
        this.id = id;
        this.author = author;
        this.name = name;
        this.genre=genre;
        this.pub_date=pub_date;
        
    }
    
    
    public Reservation startReservationManagement(int nextID,User u){
        return this.strategy_pattern.startReservationManagement(nextID,this,u);
    }
    
    /**
     * @return the author
     */
    public Author getAuthor() {
        return author;
    }

    /**
     * @param author the author to set
     */
    public void setAuthor(Author author) {
        this.author = author;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the genre
     */
    public String getGenre() {
        return genre;
    }

    /**
     * @param genre the genre to set
     */
    public void setGenre(String genre) {
        this.genre = genre;
    }

    /**
     * @return the pub_date
     */
    public Date getPub_date() {
        return pub_date;
    }

    /**
     * @param pub_date the pub_date to set
     */
    public void setPub_date(Date pub_date) {
        this.pub_date = pub_date;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the count
     */
    public long getCount() {
        return count;
    }

    /**
     * @param count the count to set
     */
    public void setCount(long count) {
        this.count = count;
    }
    
    
    
}
