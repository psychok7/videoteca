/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package design_patterns.strategy;

import beans.Reservation;
import beans.User;

/**
 *
 * @author root
 */
    interface IReservation {
        Reservation startReservationManagement(int nextID,Product product,User u);
    }

    //Context

