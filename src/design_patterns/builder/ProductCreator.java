/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package design_patterns.builder;

import beans.Author;
import design_patterns.strategy.Product;
import java.util.Date;

/**
 *
 * @author root
 */
public class ProductCreator {
    
    private ProductBuilder productbuilder;
    
    
    public void constructProduct(int id, Author author, String name, String genre, Date date) {
        this.productbuilder.createNewProduct();
        this.productbuilder.buildId(id);
        this.productbuilder.buildAuthor(author);
        this.productbuilder.buildName(name);
        this.productbuilder.buildGenre(genre);
        this.productbuilder.buildPubDate(date);
    }
    
    public Product getProduct() {
        return productbuilder.getProduct();
    }


    /**
     * @param product the product to set
     */
    public void setProduct(ProductBuilder product) {
        this.productbuilder = product;
    }
    
}
