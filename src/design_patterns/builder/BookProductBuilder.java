/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package design_patterns.builder;

import beans.Author;
import java.util.Date;

/**
 *
 * @author root
 */
/** "ConcreteBuilder" */
public class BookProductBuilder extends ProductBuilder{

    @Override
    public void buildId(int id) {
        product.setId(id);
    }

    @Override
    public void buildAuthor(Author author) {
        product.setAuthor(author);
    }

    @Override
    public void buildName(String name) {
        product.setName(name);
    }

    @Override
    public void buildGenre(String genre) {
        product.setGenre(genre);
    }

    @Override
    public void buildPubDate(Date date) {
        product.setPub_date(date);
    }
    
}
