/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package design_patterns.builder;

import beans.Author;
import design_patterns.strategy.Product;
import java.util.Date;

/**
 *
 * @author root
 */
/** "Abstract Builder" */
abstract class ProductBuilder {
    protected Product product;

    /**
     * @return the product
     */
    public Product getProduct() {
        return product;
    }

    /**
     * @param product the product to set
     */
    public void setProduct(Product product) {
        this.product = product;
    }
    
    public void createNewProduct() { product = new Product(); }
    public abstract void buildId(int id);
    public abstract void buildAuthor(Author author);
    public abstract void buildName(String name);
    public abstract void buildGenre(String genre);
    public abstract void buildPubDate(Date date);
    
    
}
